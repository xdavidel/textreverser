﻿using System;
using System.Text;
using System.Windows;

namespace TextReverser
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		private const string ENGLISH = "en-US";
		private const string HEBREW = "he-IL";

		private const string PATH = "..\\Resource\\";

		public MainWindow()
		{
			InitializeComponent();
			SetLanguageDictionary();
		}

		private void SetLanguageDictionary(string culture = "")
		{
			ResourceDictionary dictionary = new ResourceDictionary();
//			if (culture.Equals(""))
//			{
//				culture = Thread.CurrentThread.CurrentCulture.ToString();
//			}


			switch (culture)
			{
				case ENGLISH:
					dictionary.Source = new Uri($"{PATH}Dictionary-en.xaml",UriKind.Relative);
					break;
				case HEBREW:
					dictionary.Source = new Uri($"{PATH}Dictionary-he.xaml", UriKind.Relative);
					break;
				default:
					dictionary.Source = new Uri($"{PATH}Dictionary-he.xaml", UriKind.Relative);
					break;
			}
			Resources.MergedDictionaries.Add(dictionary);
		}

		private void MenuItem_OnClick_English_Language(object sender, RoutedEventArgs e)
		{
			SetLanguageDictionary(ENGLISH);
		}

		private void MenuItem_OnClick_Hebrew_Language(object sender, RoutedEventArgs e)
		{
			SetLanguageDictionary(HEBREW);
		}

		private void ReverseButton_OnClick(object sender, RoutedEventArgs e)
		{
			string inputText = textBox.Text;
			if (inputText.Equals("")) return;

			StringBuilder outputText = new StringBuilder();
			for (int i = inputText.Length - 1; i >= 0; i--)
			{
				outputText.Append(inputText[i]);
			}

			textBox.Text = outputText.ToString();
		}

		private void MenuItem_OnClick_Exit(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
